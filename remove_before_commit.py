#!/usr/bin/env python
import os, sys
paths = [
    '/home/paul/cvs/rtf2xml/rtf2xml/*pyc',
    'dist',
    'build',
    'error',
    'temp*',
    'example_files/xml_files/test.xml',
    'example_files/xml_files/test.rtf',
    'docs/tags/xml/test.html',
    'docs/tags/structure/temp.xml',
    'docs/tags/structure/elements_temp_dir',
    'docs/tags/structure/TEMP_XML.xml',
    'docs/convert_scripts/validation_temp',
    'docs/convert_scripts/toc_temp.xml',
    'docs/convert_scripts/rearranged_temp.xml',
    'docs/convert_scripts/numbered_temp.xml',
    'docs/convert_scripts/man_temp.xml',
    'docs/convert_scripts/elements_temp_dir',
    'docs/convert_scripts/block.xml',
    'docs/convert_scripts/arranged_temp.xml',
    'docs/convert_scripts/TEMP_XML.xml',
    'docs/convert_scripts/TEMP_ELEMENT.xml',
    'developer_tools/test.txt',
    'developer_tools/temp.xml',
    'xsl_stylesheets/xml_to_rtf/test.xml',
    'xsl_stylesheets/xml_to_rtf/test.html',
    'xsl_stylesheets/docbook/to_sdoc_old.xsl',
    'scripts/encode_to_rtf.pyc',
    'rtf2xml/rtf2xml_debug_dir',
    '/home/paul/cvs/rtf2xml/docs/convert_scripts/rtf2xml.1',
    '/home/paul/cvs/rtf2xml/docs/convert_scripts/rtf2xml_copy.1',




]

path_string = ' '.join(paths)
command = 'rm -Rf %s' % path_string
os.system(command)


