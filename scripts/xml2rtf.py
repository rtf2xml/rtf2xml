#!/usr/bin/env python



"""

This simple script converts an XML document to RTF.


Use
===

python xml2rtf.py <in-file>

The script outputs to the terminal.



"""

# The script executes two processes:

# 1. Convert the script to RTF using an xslt stylesheet

# 2. Process the resulting file with  encode_to_rtf, 
#    which changes unicode characters above 127 to 
#    the RTF equivelents


#########################################################
# User Defined variables
# 
# Location of the xslt-stylehseet
xsl_location = '/home/paul/cvs/rtf2xml/xsl_stylesheets/xml2rtf.xsl'


# processor
# The xsltp processor to use. Use xsltproc, xalan, or saxon
processor = 'xsltproc'


# End User Defined variables
#########################################################

import os, sys, commands, tempfile


try:
    import encode_to_rtf
except ImportError:
    sys.stderr.write('Cannot find module encode_to_rtf\n')
    sys.stderr.write('This module was in the scripts directory\n')
    sys.exit(1)

# check to make sure xsl_location really exists
if not os.path.isfile(xsl_location):
    sys.stderr.write('Cannot find stylesheet "%s" in rtf2sdoc\n' % xsl_location)
    sys.stderr.write('\nPlease fix the location of the stylesheet\n'
            'in the rtf2sdoc script\n')
    sys.exit(1)

# get file to work on

length = len(sys.argv)
if length != 2:
    sys.stderr.write('Please provide one and only one file to work on\n')
    sys.exit(1)
in_file = sys.argv[1]

# check if file really exists
if not os.path.isfile(in_file):
    sys.stderr.write('File "%s" does not exist\n' % in_file)
    sys.exit(1)


# make a temporary file to output the raw XML to
temp_file = tempfile.mktemp()


# Convert to sdoc using an xslt processor.

# Determine command according to xslt processor
if processor == 'xsltproc':
    xsl_command = 'xsltproc --novalid -o %s %s %s' % (temp_file, xsl_location, in_file)
elif processor == 'xalan':
    xsl_command = 'java org.apache.xalan.xslt.Process  -in %s -xsl %s -out' %\
        (in_file, xsl_location, temp_file)
elif processor == 'saxon':
    xsl_command = 'java  com.icl.saxon.StyleSheet  -o %s %s %s' %\
        (temp_file, in_file, xsl_location)
else:
    sys.stderr.write('Do not know how to hanlde "%s"\n' % processor)
    sys.stderr.write('Script will now quit\n')

# Now execute process.
os.system(xsl_command)



# encode the text
#
encode_obj = encode_to_rtf.Encode()
encode_obj.encode_file(temp_file)


# remove temporary file
try:
    os.remove(temp_file)
except OSError:
    pass


