import os, re, sys, codecs

import xml.sax.saxutils
import xml.sax

# turn on this line if you want to disablenamespaces
from xml.sax.handler import feature_namespaces



"""

PURPOSE
=======

wrap repeated elements:

<paragraph-definition name = "quote">

    <para>text</para>
</paragraph-definition>

<paragraph-definition name = "quote">

    <para>more text</para>
</paragraph-definition>

<paragraph-definition name = "quote">

    <para>still more text</para>
</paragraph-definition>


<paragraph-definition name = "Normal">

    <para>still more text</para>
</paragraph-definition>


<paragraph-definition name = "Normal">

    <para>still more text</para>
</paragraph-definition>

Becomes:


<wrap-paragraph-definition>
<paragraph-definition name = "quote">

    <para>text</para>
</paragraph-definition>

<paragraph-definition name = "quote">

    <para>more text</para>
</paragraph-definition>

<paragraph-definition name = "quote">

    <para>still more text</para>
</paragraph-definition>
</wrap-paragraph-defintion>


<wrap-paragraph-definition>
<paragraph-definition name = "Normal">

    <para>still more text</para>
</paragraph-definition>


<paragraph-definition name = "Normal">

    <para>still more text</para>
</paragraph-definition>
</wrap-paragraph-definition>




Use
===

Here is an example:
    the_element_name = 'my_element_name'
    the_att_dict = {'att':'value'}
    the_group_obj = GroupTree(
        in_file = input_file,
        out_file = output_file,
        element_name = the_element_name,
        att_dict = the_att_dict
            )
    result = the_group_obj.group()
    if result == 0:
        print 'File well-formed'
    else:
        print 'Sorry, file not well-formed'


This would find group together all elements that looked like:

    <my_element_name att="value">


You probably want to group together all elements with the same name and 
with the same value, as in the example above. In this case, use:

    the_element_name = 'my_element_name'
    the_att_dict = {'att':''}
    the_group_obj = GroupTree(
        in_file = input_file,
        out_file = output_file,
        element_name = the_element_name,
        att_dict = the_att_dict
            )
    result = the_group_obj.group()
    if result == 0:
        print 'File well-formed'
    else:
        print 'Sorry, file not well-formed'

Notice how the second part of the_att_dict is an empty string. This tells
the parser to group together with the same attribute.


If you want to group elements regardless of attributes, don't iclude a dictionary:

    the_element_name = 'my_element_name'
    the_group_obj = GroupTree(
        in_file = input_file,
        out_file = output_file,
        element_name = the_element_name,
            )
    result = the_group_obj.group()
    if result == 0:
        print 'File well-formed'
    else:
        print 'Sorry, file not well-formed'


    




"""


class Group(xml.sax.saxutils.DefaultHandler):
    """

    Class for handling the XML file. SAX uses the methods in this class to
    handle the data. 


    """
    def __init__(   self,  
                    write_obj,
                    element_name,
                    att_dict = {},
                
                ):

        """
        

        Requires:

            write_obj -- a write object

        Returns:

            nothing


        Logic:

            Set the necessary parameters:

                self.__write_obj --the object for outputing text

                self.__name --name of current element

                self.__character --string containg characters in current
                element.

        """
        self.__write_obj = write_obj
        self.__name = ''
        self.__character = ''
        self.__element_name = element_name
        self.__att_dict = att_dict
        self.__desired_att = []
        self.__found_start_element = []
        self.__found_end_element = []
        self.__nest = 0
    
    def startElement(self, name, attrs):
        """

        Logic:

            The SAX driver uses this function when if finds a beginning tag.




        """
        if len(self.__found_end_element)> 0 and self.__found_end_element[-1]:
            self.__start_element_after_end(name, attrs)

        elif len(self.__found_start_element) > 0 and self.__found_start_element[-1]:
            self.__start_element_nest(name, attrs)

        else:
            self.__start_element_default(name, attrs)
                    
    
    def __start_element_after_end(self, name, attrs):
        match = self.__target_element_match(name, attrs)
        # don't continue with wrap
        if match != 0:
            self.__write_select_end_element()
            possible = self.__target_element_possible(name, attrs)
            if possible == 0:
                self.__write_start_wrap(attrs)
                self.__write_start_element(name, attrs)
            else:
                if name == self.__element_name:
                    self.__found_start_element.append(0)
                self.__write_start_element(name, attrs)

        #  continue with wrap
        else:
            self.__write_start_element(name, attrs)
            self.__found_end_element[-1] = 0
    
    def __start_element_nest(self, name, attrs):
        if name == self.__element_name:
            possible = self.__target_element_possible(name, attrs)
            if possible == 0:
                self.__write_start_wrap(attrs)
                self.__write_start_element(name, attrs)
            else:
                self.__found_start_element.append(0)
                self.__write_start_element(name, attrs)
                
        else:
            self.__write_start_element(name, attrs)


    def __start_element_default(self, name, attrs):
        possible = self.__target_element_possible(name, attrs)
        if possible == 0:
            self.__write_start_wrap(attrs)
        elif name == self.__element_name:
            self.__found_start_element.append(0)
        self.__write_start_element(name, attrs)
            
    

    def __target_element_possible(self, name, attrs):
        the_keys = self.__att_dict.keys()
        if name == self.__element_name:
            for the_key in the_keys:
                desired_att = self.__att_dict[the_key]
                current_att = attrs.get(the_key)
                if current_att == None:
                    return 1
                elif desired_att == '':
                    pass
                elif desired_att != current_att:
                    return 1
            return 0
        else:
            return 1
        
    def __target_element_match(self, name, attrs):
        if len(self.__desired_att) == 0:
            return 1
        if name == self.__element_name:
            the_keys = self.__desired_att[-1].keys()
            for the_key in the_keys:
                desired_att = self.__desired_att[-1][the_key]
                current_att = attrs.get(the_key)
                if desired_att != current_att:
                    return 1
            return 0
        else:
            return 1
    def __reset_dict(self, attrs):
        """

        Set the current attributes of the element (attrs) to the 
        last dictionary in self.__desired_att


        """
        self.__desired_att.append({})
        the_keys = self.__att_dict.keys()
        for the_key in the_keys:
            current_att = attrs.get(the_key)
            self.__desired_att[-1][the_key] = current_att
        
    def __write_start_element(self, name, attrs):
        open_tag = '<%s' % name
        keys = attrs.keys()
        for key in keys:
            att = key
            value = attrs[key]
            value = self.__escape_characters(value)
            value = value.replace('"', '&quot;')
            open_tag += ' %s="%s"' % (att, value)
        open_tag += '>'
        self.__write_obj.write(open_tag)

    def __write_start_wrap(self, atts):
        """
        Write a wrapper tag
        self.__att_dict = att_dict

        """

        self.__write_obj.write('<wrap-%s' % self.__element_name)
        for att in self.__att_dict:
            value = atts.get(att)
            if value:
                value = self.__escape_characters(value)
                self.__write_obj.write(' %s="%s" ' % (att, value))
        self.__write_obj.write('>')
        self.__found_start_element.append(1)
        self.__found_end_element.append(0)
        self.__reset_dict(atts)

    def __write_select_end_element(self):
        self.__write_obj.write('</wrap-%s>' % self.__element_name)

        if len(self.__found_end_element) > 0:
            self.__found_end_element.pop()
            self.__desired_att.pop()
            self.__found_start_element.pop()
    
    def characters(self, character):
        """

        Logic:

            The SAX driver uses this function when it finds text.

        """

        character = self.__escape_characters(character)
        self.__write_obj.write(character)



            
    
    def __escape_characters(self, character):
        """

        Method for escaping text

        """
        character = character.replace('&', '&amp;')
        character = character.replace('<', '&lt;')
        character = character.replace('>', '&gt;')
        return character
            
    
    def endElement(self, name):
        """

        Logic:

            The SAX driver uses the function when it finds an end tag. It
            pases to this function the name of the end element.

        """

        if len(self.__found_end_element) > 0 and self.__found_end_element[-1]:
            self.__end_element_after_end(name)
        # in the tree
        elif len(self.__found_start_element) > 0 and self.__found_start_element[-1]:
            self.__end_element_in_tree(name)
        elif name == self.__element_name:
            self.__end_element_not_in_tree(name)
        else:
            self.__write_obj.write('</%s>' % name)


            
    def __end_element_after_end(self, name):
        """

        The selected end element was the *previous* element. You have found
        another end element.

        Start an undendng loop. For each unclosed element, close out the wrapper.

        Check to see if the current element name is also the selected end element.
        If it is, set the last number in the 1.

        """
        while 1:
            self.__write_select_end_element()
            self.__write_obj.write('</%s>' % name)
            if len(self.__found_end_element) > 0 and not self.__found_end_element[-1]:
                break
            elif len(self.__found_end_element) == 0:
                break
        if len(self.__found_start_element) > 0 and self.__found_start_element[-1]:
            if name == self.__element_name:
                self.__found_end_element[-1] = 1


    def __end_element_in_tree(self, name):
        """

        You are in the tree, which is why this method has been called.

        Check to see if the end element of the tree is found. If so, 
        set a flag to true so the next element, whether beginning or end, can 
        be checked.

        If the end element isn't the selected one, simply print out the 
        element name.


        """
        if name == self.__element_name:
            self.__found_end_element[-1] = 1
        self.__write_obj.write('</%s>' % name)

    def __end_element_not_in_tree(self, name):

        """
        You have found the element that matches the start element. However, 
        you are not in the tree because of attributes. Pop the start and 
        end element lists. Write out the tag.


        """
        if len(self.__found_end_element) > 1:
            self.__found_end_element.pop()
            self.__found_start_element.pop()
        self.__write_obj.write('</%s>' % name)


class GroupTree:

    def __init__(   self, 
                    in_file, 
                    out_file,
                    element_name,
                    att_dict = {},
                    debug_dir = None,
                    system_dtd = None,
                    ):


        """
        

        Requires:

            file --file to be read

            output --file to output to

        
        Returns:

            Nothing. Outputs a file

        Logic:

            Set up a write object. 

            Create an instance of the InlineHandler for sax to use.

            Pass this instance to the SAX driver.

            Use the SAX driver to handle the file. 


        """
        self.__output = out_file 
        self.__input = in_file
        self.__element_name = element_name
        self.__att_dict = att_dict
        self.__debug_dir = debug_dir
        self.__system_dtd = system_dtd

    def group(self):
        (utf8_encode, utf8_decode, utf8_reader, utf8_writer) = codecs.lookup("utf-8")
        write_obj = utf8_writer(open(self.__output, 'w'))
        parser = xml.sax.make_parser()
        parser.setFeature("http://xml.org/sax/features/external-general-entities", False)
        parser.setFeature(feature_namespaces, 0)
        group_handler = Group( 
                write_obj = write_obj,
                element_name = self.__element_name,
                att_dict = self.__att_dict,
                )
        parser.setContentHandler(group_handler)

        # write XML DTD
        write_obj.write('<?xml version="1.0"?>\n')
        if self.__system_dtd:
            write_obj.write('<!DOCTYPE doc SYSTEM "%s">\n' % self.__system_dtd)
        parser.parse(self.__input)             
        write_obj.close()



        parser = xml.sax.make_parser()
        parser.setFeature(feature_namespaces, 0)
        parser.setFeature("http://xml.org/sax/features/external-general-entities", False)

        if self.__debug_dir:
            debug_file = os.path.join(self.__debug_dir, 'sax_wrap.xml')
            self.__output_xml(self.__output, debug_file)
        try:
            parser.parse(self.__output)             
            return 0
        except xml.sax._exceptions.SAXParseException:
            return 1

    def __output_xml(self, in_file, out_file):
        """

        output the ill-formed xml file

        """
            
        (utf8_encode, utf8_decode, utf8_reader, utf8_writer) = codecs.lookup("utf-8")
        write_obj = utf8_writer(open(out_file, 'w'))
        write_obj = open(out_file, 'w')
        read_obj = utf8_writer(open(in_file, 'r'))
        read_obj = open(in_file, 'r')
        line = 1
        while line:
            line = read_obj.readline()
            if isinstance(line, type(u"")):
                line = line.encode("utf-8")
            write_obj.write(line)
        
        read_obj.close()
        write_obj.close()

            
if __name__ == '__main__':
    if len(sys.argv) != 3:
        sys.stderr.write('Please provide both an input and output file:\n')
        sys.stderr.write('python sax_copy.py output_file input_file\n')
        sys.exit(1)
    input_file = sys.argv[2]
    output_file = sys.argv[1]
    the_element_name = 'paragraph-definition'
    # the_att_dict = {'bogus':'', 'bogus2':''}
    # the_att_dict = {'name':'test'}
    the_att_dict = {'name':''}
    the_group_obj = GroupTree(
        in_file = input_file,
        out_file = output_file,
        element_name = the_element_name,
        att_dict = the_att_dict
            )
    result = the_group_obj.group()
    if result == 0:
        print 'File well-formed'
    else:
        print 'Sorry, file not well-formed'




