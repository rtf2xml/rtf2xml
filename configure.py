# /usr/bin/env python 

import sys, os
import rtf2xml.options_trem
import rtf2xml.configure_txt

"""

The configuration script gets the target from the command line. It creates a
file with the configuration variable, and a short script for the rest of
script to be able to read and locate he configuration files.

"""


no_pyxml = 0
no_xslt_proc = 0

def get_options_func():

    options_dict = {
        'nothing-here'      : [0],
    }
            
    options_obj = rtf2xml.options_trem.ParseOptions(
                    system_string = sys.argv,
                    options_dict = options_dict
                )

    options, arguments = options_obj.parse_options()
    return options, arguments

def configure():
    sys.stdout.write('\n')
    options, arguments = get_options_func()
    the_keys = options.keys()
    options_dict = get_options()
    check_target(options_dict)
    make_location(options_dict) 
    make_var_file(options_dict)



def get_options():
    configure_obj = rtf2xml.configure_txt.Configure(
                configuration_file = 'configure.txt')
    options_dict = configure_obj.get_configuration(type = 'setup')
    return options_dict

    options_dict = configure_obj.get_configuration(type = 'normal')

def check_target(options_dict):
    target = options_dict.get('configure-directory')
    if target == None:
        sys.stderr.write('You have not provided a directory in the \n'
                '"<configuration-directory>" in congiguration.xml.\n'
                'Please do this and then run setup again.\n'
            )
        sys.exit(1)
    the_dir = os.path.join(target, 'rtf2xml_data')
    if os.path.isdir(the_dir):
        sys.stdout.write('Directory "%s" already exists.\n'
                'This will be the directory where your data is stored.\n\n'
                % the_dir
            )
        return
    try:
        os.mkdir(the_dir)
        sys.stdout.write('Directory "%s" will be the directory where your data '
                'is stored.\n\n' % the_dir)
    except OSError, msg:
        sys.stderr.write(str(msg))
        sys.stderr.write('\n')
        sys.stderr.write('Could not make directory.\n')
        sys.stderr.write('Please re-run this script again with an appropriate target.\n')
        sys.exit(1)
    

def check_xslt_proc(options_dict):
    if no_xslt_proc:
        return
    xslt_proc = options_dict.get('xslt-processor')
    if xslt_proc == None:
        sys.stderr.write('You have not provided an xslt-processor in "configure.xml."\n'
                'Please provide an xslt processor and then run setup again.\n'
                )
        if not use_xslt:
            sys.stderr.write(
                'If you do not wish to use an XSLT processor, use the "--no-xslt-proc"\n'
                'option when you run this setup:\n'
                'python configure.py --no-xslt-proc.\n'
                )
        sys.exit(1)
    cwd = os.getcwd()
    test_xml = os.path.join(cwd, 'test_files', 'xsl_proc_test.xml')
    test_xsl = os.path.join(cwd, 'test_files', 'xsl_proc_test.xsl')
    test_out = os.path.join(cwd, 'test_files', 'xsl_proc_test_out.xml')
    trans_obj = rtf2xml.transform.xsl_convert.XslConvert(
        processor = xslt_proc
        )
    sys.stdout.write('Testing xslt processor "%s"...\n' % xslt_proc)
    exit_status, out_text = trans_obj.transform(
        file = test_xml,
        xsl_file = test_xsl,
        output = test_out,
        )
    if exit_status == 0:
        sys.stdout.write('"%s" appears to be working on your system.\n' % xslt_proc)
    else:
        sys.stderr.write('%s" does not appear to be working on your system.\n'
                'Please correct this and then re-run setup.\n'
                )


def default_target():
    sys.stdout.write('using default \'/etc\' for the configuration directory\n')
    return '/etc'


def make_var_file(options_dict):
    target = options_dict['configure-directory']
    write_obj = open('var_file', 'w')
    # write_obj.write('[global]\n')
    write_obj.write(target)
    write_obj.close()
    

def make_location(options_dict):
    target = options_dict['configure-directory']
    the_dir = os.path.join(target, 'rtf2xml_data')
    write_obj = open('rtf2xml/configuration_dir.py', 'w')
    write_obj.write(
    """
def get_dir():
    return '%s'


    """
    % the_dir)


if __name__ == '__main__':
    configure()

