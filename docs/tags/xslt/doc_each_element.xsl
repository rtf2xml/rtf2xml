<xsl:stylesheet 
  xmlns:xsl = "http://www.w3.org/1999/XSL/Transform"
  version = "1.0"
>
    <xsl:import href = "/home/paul/Documents/data/xsl_style_sheets/my_docbook/sdoc_block.xsl"/>
    <xsl:import href = "/home/paul/Documents/data/xsl_style_sheets/my_docbook/sdoc_inline.xsl"/>
    <xsl:import href = "/home/paul/Documents/data/xsl_style_sheets/my_docbook/sdoc_tables.xsl"/>


    <xsl:template match = "/article/section[@role='elements']/section[@role='element']/title"/>

    <xsl:template name = "make-each-element">
        <xsl:apply-templates 
            select = "/article/section[@role='elements']/section[@role='element']" 
            mode = "each-element"
        />
    </xsl:template>

    <xsl:template match = "/article/section[@role='elements']/section[@role='element']" mode = "each-element">
        <xsl:variable name = "title" select = "title"/>
       <xsl:document 
            href = "/home/paul/cvs/rtf2xml/docs/html/elements/{$title}.html"

            method = "xml"
            
            >
        <html>
            <head>
                <title>
                    <xsl:value-of select="title"/>
                </title>
            </head>
            <body>
                <xsl:call-template name = "do-element"/>
                <p class = "home">
                    <a href = "../index.html">
                        <xsl:text>home | </xsl:text>
                    </a>
                    <a href = "elements-index.html">
                        <xsl:text>index of elements</xsl:text>
                    </a>
                </p>
            </body>
        </html>
       </xsl:document>
    </xsl:template>

    <xsl:template name = "do-element">
        <xsl:apply-templates select = "title" mode = "top-of-page"/>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match = "/article/section[@role='elements']/section[@role='element']/title"
        mode = "top-of-page">
        <h1 class = "title-top-of-page">
            <xsl:apply-templates/>
        </h1>
        <hr />
    </xsl:template>

    <!--sections-->

    <xsl:template match = "section[@role='synopsis']/title">
        <h2 class = "synopsis">
            <xsl:apply-templates/>
        </h2>
    </xsl:template>


    <xsl:template match = "section[@role='synopsis']/section[@role='content-model']/title|
        section[@role='synopsis']/section[@role='mixed-content-model']/title">
        <h3 class = "model">
            <xsl:apply-templates/>
        </h3>
    </xsl:template>

    <xsl:template match = "section[@role='synopsis']/section[@role='attributes']/title">
        <h3 class = "attributes">
            <xsl:apply-templates/>
        </h3>
    </xsl:template>


    <xsl:template match = "section[@role='synopsis']/section[@role='attributes']/table/title"/>


    <xsl:template match = "section[@role='description']/title">
        <h2 class = "synopsis">
            <xsl:apply-templates/>
        </h2>
    </xsl:template>

    <xsl:template match = "section[@role='description']/section[@role='attributes-in-description']/title">
        <h3 class = "attributes-in-description">
            <xsl:apply-templates/>
        </h3>
    </xsl:template>


    <xsl:template match = "section[@role='description']/section[@role='children']/title">
        <h3 class = "attributes-in-description">
            <xsl:apply-templates/>
        </h3>
    </xsl:template>


    <xsl:template match = "section[@role='description']/section[@role='parents']/title">
        <h3 class = "attributes-in-description">
            <xsl:apply-templates/>
        </h3>
    </xsl:template>

    <xsl:template 
    match = "section[@role='description']/section[@role='attributes-in-description']/section[@role='attribute-in-description']/title">
        <h4 class = "attribute-in-description">
            <xsl:apply-templates/>
        </h4>
    </xsl:template>

    <xsl:template match = "section[@role='example']/title">
        <h2 class = "example">
            <xsl:apply-templates/>
        </h2>
    </xsl:template>

    <xsl:template match = "section[@role='example']/example[@role='programlisting']/title"/>


</xsl:stylesheet>
