#!/usr/bin/env python

import os, sys


xslt_dir = '/home/paul/cvs/rtf2xml/docs/xslt'
doc_dir = '/home/paul/cvs/rtf2xml/docs/xml'
html_dir = '/home/paul/cvs/rtf2xml/docs/html'
cvs_dir = '/home/paul/cvs/rtf2xml'


print 'putting together separte docs...'
command = 'xsltproc %s/put_together_for_html.xsl %s/master.xml \
     > arranged_temp.xml' % (xslt_dir, doc_dir)
os.system(command)

print 'converting to text...'
command = 'xsltproc %s/notes_to_block_txt.xsl arranged_temp.xml  > block_notes.xml' % (xslt_dir)
os.system(command)
     
command = 'xml2txt --output notes_as_text block_notes.xml'
print command
os.system(command)
