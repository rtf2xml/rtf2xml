#!/usr/bin/env python
import os, sys

class Convert:

    def __init__(self):
        self.__main_xslt_sheet = '/home/paul/cvs/rtf2xml/docs/tags/xslt/doc_to_html.xsl'
        self.__doc = '/home/paul/cvs/rtf2xml/docs/tags/xml/elements_index.xml'

    def convert(self):
        command = 'xsltproc  %s %s > /dev/null' % (self.__main_xslt_sheet, self.__doc)
        os.system(command)

def Main():
    convert_obj = Convert()
    convert_obj.convert()

if __name__ == '__main__':
    Main()
