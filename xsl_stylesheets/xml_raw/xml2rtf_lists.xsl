<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:rtf="http://rtf2xml.sourceforge.net/">
    <xsl:output method="text"/>

    <xsl:template match="rtf:list-table">
        <xsl:text>{\*\listtable&#xA;</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>&#xA;}</xsl:text>
    </xsl:template>

    <xsl:template match="rtf:list-in-table">
        <xsl:text>{\list&#xA;</xsl:text>
        <xsl:text>\listid</xsl:text>
        <xsl:value-of select="@list-table-id"/>
        <xsl:text>\listtemplateid</xsl:text>
        <xsl:value-of select="@list-template-id"/>
        <xsl:text>\listhybrid</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>{\listname ;}</xsl:text>
        <xsl:text>}&#xA;</xsl:text>

    </xsl:template>

    <xsl:template match="rtf:level-in-table">
        <xsl:text>{\listlevel&#xA;</xsl:text>
        <xsl:if test="@level-template-id">
            <xsl:text>\leveltemplateid</xsl:text>
            <xsl:value-of select="@level-template-id"/>
        </xsl:if>
        <xsl:if test="@level-indent">
            <xsl:text>\levelindent</xsl:text>
            <xsl:value-of select="round(@level-indent * 20)"/>
        </xsl:if>
        <xsl:if test="@list-space">
            <xsl:text>\levelspace</xsl:text>
            <xsl:value-of select="round(@list-space * 20)"/>
        </xsl:if>
        <xsl:if test="@left-indent">
            <xsl:text>\li</xsl:text>
            <xsl:value-of select="round(@left-indent * 20)"/>
        </xsl:if>
        <xsl:if test="@first-line-indent">
            <xsl:text>\fi</xsl:text>
            <xsl:value-of select="round(@first-line-indent * 20)"/>
        </xsl:if>
        <xsl:if test="@paragraph-style-name">
            <xsl:text>\s</xsl:text>
            <xsl:value-of select="@paragraph-style-name"/>
        </xsl:if>
        <xsl:if test="@bold='true'">
            <xsl:text>\b</xsl:text>
        </xsl:if>
        <xsl:if test="@italics='true'">
            <xsl:text>\i</xsl:text>
        </xsl:if>

        <!--22 for bullet, etc-->
        <xsl:call-template name = "convert-level-number-type"/>

        <xsl:text>&#xA;{\leveltext</xsl:text>
        <xsl:choose>
            <xsl:when test = "@numbering-type = 'bullet'">
                <xsl:call-template name = "level-text-bullet"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="level-text"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>;}&#xA;</xsl:text>
        <xsl:text>&#xA;{\levelnumbers</xsl:text>
        <xsl:call-template name = "do-level-numbers"/>
        <xsl:text>}&#xA;</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}&#xA;</xsl:text>
    </xsl:template>


<xsl:template name = "do-level-numbers">
    <xsl:call-template name = "recursive-level-numbers">
        <xsl:with-param name = "level" select = "@level"/>
    </xsl:call-template>
    <xsl:text>;</xsl:text>
</xsl:template>






<xsl:template name = "recursive-level-numbers">
    <xsl:param name = "level"/>
    <xsl:param name = "previous-string">0</xsl:param>
    <xsl:param name = "real-string"/>
    <xsl:param name = "which-att">1</xsl:param>
    <xsl:choose>
        <xsl:when test = "$which-att = '10'">
            <xsl:value-of select = "$real-string"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:variable name = "this-string">
            <xsl:choose>
                <xsl:when test = "$which-att = '1'">
                    <xsl:if test = "@show-level1">
                        <xsl:text>\&#x0027;</xsl:text>
                        <xsl:call-template name = "dec-to-hex">
                            <xsl:with-param name = "value" select = "string-length(@level1-prefix) + 1"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:when test = "$which-att = '2'">
                    <xsl:if test = "@show-level2 ">
                        <xsl:text>\&#x0027;</xsl:text>
                        <xsl:variable name = "len">
                            <xsl:value-of select = "string-length(@level2-prefix) + string-length(@level1-suffix) + 
                              $previous-string + 1"/>
                        </xsl:variable>
                        <xsl:call-template name = "dec-to-hex">
                            <xsl:with-param name = "value" select = "$len"/>
                        </xsl:call-template>
                    </xsl:if>
                    
                </xsl:when>

                <xsl:when test = "$which-att = '3'">
                    <xsl:if test = "@show-level3 ">
                        <xsl:text>\&#x0027;</xsl:text>
                        <xsl:variable name = "len">
                            <xsl:value-of select = "string-length(@level3-prefix) + string-length(@level2-suffix) + 
                              $previous-string + 1"/>
                        </xsl:variable>
                        <xsl:call-template name = "dec-to-hex">
                            <xsl:with-param name = "value" select = "$len"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:when test = "$which-att = '4'">
                    <xsl:if test = "@show-level4 ">
                        <xsl:text>\&#x0027;</xsl:text>
                        <xsl:variable name = "len">
                            <xsl:value-of select = "string-length(@level4-prefix) + string-length(@level3-suffix) + 
                              $previous-string + 1"/>
                        </xsl:variable>
                        <xsl:call-template name = "dec-to-hex">
                            <xsl:with-param name = "value" select = "$len"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:when test = "$which-att = '5'">
                    <xsl:if test = "@show-level5 ">
                        <xsl:text>\&#x0027;</xsl:text>
                        <xsl:variable name = "len">
                            <xsl:value-of select = "string-length(@level5-prefix) + string-length(@level4-suffix) + 
                              $previous-string + 1"/>
                        </xsl:variable>
                        <xsl:call-template name = "dec-to-hex">
                            <xsl:with-param name = "value" select = "$len"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:when test = "$which-att = '6'">
                    <xsl:if test = "@show-level6 ">
                        <xsl:text>\&#x0027;</xsl:text>
                        <xsl:variable name = "len">
                            <xsl:value-of select = "string-length(@level6-prefix) + string-length(@level5-suffix) + 
                              $previous-string + 1"/>
                        </xsl:variable>
                        <xsl:call-template name = "dec-to-hex">
                            <xsl:with-param name = "value" select = "$len"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:when test = "$which-att = '7'">
                    <xsl:if test = "@show-level7 ">
                        <xsl:text>\&#x0027;</xsl:text>
                        <xsl:variable name = "len">
                            <xsl:value-of select = "string-length(@level7-prefix) + string-length(@level6-suffix) + 
                              $previous-string + 1"/>
                        </xsl:variable>
                        <xsl:call-template name = "dec-to-hex">
                            <xsl:with-param name = "value" select = "$len"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:when test = "$which-att = '8'">
                    <xsl:if test = "@show-level8 ">
                        <xsl:text>\&#x0027;</xsl:text>
                        <xsl:variable name = "len">
                            <xsl:value-of select = "string-length(@level8-prefix) + string-length(@level7-suffix) + 
                              $previous-string + 1"/>
                        </xsl:variable>
                        <xsl:call-template name = "dec-to-hex">
                            <xsl:with-param name = "value" select = "$len"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:when test = "$which-att = '9'">
                    <xsl:if test = "@show-level9 ">
                        <xsl:text>\&#x0027;</xsl:text>
                        <xsl:variable name = "len">
                            <xsl:value-of select = "string-length(@level9-prefix) + string-length(@level8-suffix) + 
                              $previous-string + 1"/>
                        </xsl:variable>
                        <xsl:call-template name = "dec-to-hex">
                            <xsl:with-param name = "value" select = "$len"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:when>

            </xsl:choose>
            </xsl:variable>

            <!--End of string variable-->
            <xsl:variable name = "length-of">
            <xsl:choose>
                <xsl:when test = "$which-att = '1'">
                    <xsl:choose>
                        <xsl:when test = "@show-level1 ">
                            <xsl:value-of select = "string-length(@level1-prefix) + 1"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>0</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test = "$which-att = '2'">
                    <xsl:choose>
                        <xsl:when test = "@show-level2 ">
                            <xsl:value-of select = "string-length(@level2-prefix) + string-length(@level1-suffix) + 1"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>0</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test = "$which-att = '3'">
                    <xsl:choose>
                        <xsl:when test = "@show-level3 ">
                            <xsl:value-of select = "string-length(@level3-prefix) + string-length(@level2-suffix) + 1"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>0</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test = "$which-att = '4'">
                    <xsl:choose>
                        <xsl:when test = "@show-level4 ">
                            <xsl:value-of select = "string-length(@level4-prefix) + string-length(@level3-suffix) + 1"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>0</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test = "$which-att = '5'">
                    <xsl:choose>
                        <xsl:when test = "@show-level5 ">
                            <xsl:value-of select = "string-length(@level5-prefix) + string-length(@level4-suffix) + 1"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>0</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test = "$which-att = '6'">
                    <xsl:choose>
                        <xsl:when test = "@show-level6 ">
                            <xsl:value-of select = "string-length(@level6-prefix) + string-length(@level5-suffix) + 1"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>0</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test = "$which-att = '7'">
                    <xsl:choose>
                        <xsl:when test = "@show-level7 ">
                            <xsl:value-of select = "string-length(@level7-prefix) + string-length(@level6-suffix) + 1"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>0</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test = "$which-att = '8'">
                    <xsl:choose>
                        <xsl:when test = "@show-level8 ">
                            <xsl:value-of select = "string-length(@level8-prefix) + string-length(@level7-suffix) + 1"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>0</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test = "$which-att = '9'">
                    <xsl:choose>
                        <xsl:when test = "@show-level9 ">
                            <xsl:value-of select = "string-length(@level9-prefix) + string-length(@level8-suffix) + 1"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>0</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
            </xsl:choose>
            </xsl:variable>
            <xsl:call-template name = "recursive-level-numbers">
                <xsl:with-param name = "level" select = "$level"/>
                <xsl:with-param name = "real-string">
                    <xsl:value-of select = "$real-string"/>
                    <xsl:value-of select = "$this-string"/>
                </xsl:with-param>
                <xsl:with-param name = "previous-string">
                    <xsl:value-of select = "$length-of + $previous-string"/>
                </xsl:with-param>
                <xsl:with-param name = "which-att" select = "$which-att + 1"/>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>
    
</xsl:template>



<xsl:template name = "length-list-text">
    <xsl:param name = "level"/>
    <xsl:variable name = "length-numbers">
        <xsl:call-template name = "num-of-show-levels">
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name = "length-text">
        <xsl:call-template name = "len-prefix-suffix"/>
    </xsl:variable>
    <xsl:value-of select = "$length-numbers + $length-text"/>
</xsl:template>



<xsl:template name = "num-of-show-levels">
    <xsl:variable name = "string">
        <xsl:if test = "@show-level1">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level2">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level3">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level4">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level5">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level6">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level7">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level8">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level9">
            <xsl:text>1</xsl:text>
        </xsl:if>
    </xsl:variable>
    <xsl:value-of select = "string-length($string)"/>
</xsl:template>

<xsl:template name = "len-prefix-suffix">
    <xsl:variable name = "all-text">
        <xsl:value-of select = "@level1-prefix"/>
        <xsl:value-of select = "@level2-prefix"/>
        <xsl:value-of select = "@level3-prefix"/>
        <xsl:value-of select = "@level4-prefix"/>
        <xsl:value-of select = "@level5-prefix"/>
        <xsl:value-of select = "@level6-prefix"/>
        <xsl:value-of select = "@level7-prefix"/>
        <xsl:value-of select = "@level8-prefix"/>
        <xsl:value-of select = "@level9-prefix"/>
        <xsl:value-of select = "@level1-suffix"/>
        <xsl:value-of select = "@level2-suffix"/>
        <xsl:value-of select = "@level3-suffix"/>
        <xsl:value-of select = "@level4-suffix"/>
        <xsl:value-of select = "@level5-suffix"/>
        <xsl:value-of select = "@level6-suffix"/>
        <xsl:value-of select = "@level7-suffix"/>
        <xsl:value-of select = "@level8-suffix"/>
        <xsl:value-of select = "@level9-suffix"/>
    </xsl:variable>
    <xsl:value-of select = "string-length($all-text)"/>
    
</xsl:template>



    


<!--level tables from original sheet-->

<!--
<paragraph-style-in-table num="0" nest-level="0" language="1033" adjust-right="true" next-style="Normal" widow-control="true" font-style="Times" name="Normal"/>

Should look like:
{\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \f4\lang1033\cgrid \snext0 Normal;}
-->



    <xsl:template name="level-text-bullet">
        <xsl:text>\'01</xsl:text>
        <xsl:choose>
            <xsl:when test = "@bullet-type">
                <xsl:value-of select = "@bullet-type"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>\u8226\'00</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="level-text">

        <xsl:text>\'</xsl:text>
        <xsl:variable name = "len-of-text">
            <xsl:call-template name = "length-list-text"/>
        </xsl:variable>
        <xsl:call-template name="dec-to-hex">
            <xsl:with-param name="value" select="$len-of-text"/>
        </xsl:call-template>
        <xsl:value-of select = "@level1-prefix"/>
        <xsl:if test = "@show-level1">
            <xsl:text>\'00</xsl:text>
        </xsl:if>
        <xsl:value-of select = "@level1-suffix"/>

        <xsl:value-of select = "@level2-prefix"/>
        <xsl:if test = "@show-level2">
            <xsl:text>\'01</xsl:text>
        </xsl:if>
        <xsl:value-of select = "@level2-suffix"/>

        <xsl:value-of select = "@level3-prefix"/>
        <xsl:if test = "@show-level3">
            <xsl:text>\'02</xsl:text>
        </xsl:if>
        <xsl:value-of select = "@level3-suffix"/>

        <xsl:value-of select = "@level4-prefix"/>
        <xsl:if test = "@show-level4">
            <xsl:text>\'03</xsl:text>
        </xsl:if>
        <xsl:value-of select = "@level4-suffix"/>

        <xsl:value-of select = "@level5-prefix"/>
        <xsl:if test = "@show-level5">
            <xsl:text>\'04</xsl:text>
        </xsl:if>
        <xsl:value-of select = "@level5-suffix"/>

        <xsl:value-of select = "@level6-prefix"/>
        <xsl:if test = "@show-level6">
            <xsl:text>\'05</xsl:text>
        </xsl:if>
        <xsl:value-of select = "@level6-suffix"/>

        <xsl:value-of select = "@level7-prefix"/>
        <xsl:if test = "@show-level7">
            <xsl:text>\'06</xsl:text>
        </xsl:if>
        <xsl:value-of select = "@level7-suffix"/>

        <xsl:value-of select = "@level8-prefix"/>
        <xsl:if test = "@show-level8">
            <xsl:text>\'07</xsl:text>
        </xsl:if>
        <xsl:value-of select = "@level8-suffix"/>

        <xsl:value-of select = "@level9-prefix"/>
        <xsl:if test = "@show-level9">
            <xsl:text>\'08</xsl:text>
        </xsl:if>
        <xsl:value-of select = "@level9-suffix"/>

        <!--
        <xsl:if test="@level1-prefix">
            <xsl:value-of select="@level1-prefix"/>
        </xsl:if>

        <xsl:if test="@level2-prefix">
            <xsl:value-of select="@level2-prefix"/>
        </xsl:if>
        <xsl:if test="@level3-prefix">
            <xsl:value-of select="@level3-prefix"/>

        </xsl:if>
        <xsl:if test="@level4-prefix">
            <xsl:value-of select="@level4-prefix"/>
        </xsl:if>
        <xsl:if test="@level5-prefix">
            <xsl:value-of select="@level5-prefix"/>
        </xsl:if>
        <xsl:if test="@level6-prefix">
            <xsl:value-of select="@level6-prefix"/>
        </xsl:if>
        <xsl:if test="@level7-prefix">
            <xsl:value-of select="@level7-prefix"/>
        </xsl:if>
        <xsl:if test="@level8-prefix">
            <xsl:value-of select="@level8-prefix"/>
        </xsl:if>
        <xsl:if test="@level9-prefix">
            <xsl:value-of select="@level9-prefix"/>
        </xsl:if>


        <xsl:if test="@level1-suffix">
            <xsl:value-of select="@level1-suffix"/>
        </xsl:if>
        <xsl:if test="@level2-suffix">
            <xsl:text>\'01</xsl:text>
            <xsl:value-of select="@level1-suffix"/>
        </xsl:if>
        <xsl:if test="@level3-suffix">
            <xsl:text>\'02</xsl:text>
            <xsl:value-of select="@level2-suffix"/>
        </xsl:if>
        <xsl:if test="@level4-suffix">
            <xsl:text>\'03</xsl:text>
            <xsl:value-of select="@level3-suffix"/>
        </xsl:if>
        <xsl:if test="@level5-suffix">
            <xsl:text>\'04</xsl:text>
            <xsl:value-of select="@level4-suffix"/>
        </xsl:if>
        <xsl:if test="@level6-suffix">
            <xsl:text>\'05</xsl:text>
            <xsl:value-of select="@level5-suffix"/>
        </xsl:if>
        <xsl:if test="@level7-suffix">
            <xsl:text>\'06</xsl:text>
            <xsl:value-of select="@level6-suffix"/>
        </xsl:if>
        <xsl:if test="@level8-suffix">
            <xsl:text>\'07</xsl:text>
            <xsl:value-of select="@level7-suffix"/>
        </xsl:if>
        <xsl:if test="@level9-suffix">
            <xsl:text>\'08</xsl:text>
            <xsl:value-of select="@level8-suffix"/>
        </xsl:if>
        -->

    </xsl:template>


    <!--is this still used?-->
    <xsl:template name="level-number">


        <xsl:if test="@level0-show-level">
            <xsl:text>\'01</xsl:text>
        </xsl:if>
        <xsl:if test="@level1-show-level">
            <xsl:text>\'03</xsl:text>
        </xsl:if>
        <xsl:if test="@level2-show-level">
            <xsl:text>\'05</xsl:text>
        </xsl:if>
        <xsl:if test="@level3-show-level">
            <xsl:text>\'07</xsl:text>
        </xsl:if>
        <xsl:if test="@level4-show-level">
            <xsl:text>\'09</xsl:text>
        </xsl:if>
        <xsl:if test="@level5-show-level">
            <xsl:text>\'0b</xsl:text>
        </xsl:if>
        <xsl:if test="@level6-show-level">
            <xsl:text>\'0d</xsl:text>
        </xsl:if>
        <xsl:if test="@level7-show-level">
            <xsl:text>\'0f</xsl:text>
        </xsl:if>
        <xsl:if test="@level8-show-level">
            <xsl:text>\'11</xsl:text>
        </xsl:if>

    </xsl:template>
    <xsl:template match="rtf:override-table">
        <xsl:text>{\*\listoverridetable&#xA;</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}</xsl:text>
    </xsl:template>

    <xsl:template match="rtf:override-list">
        <xsl:text>{\listoverride&#xA;</xsl:text>
        <xsl:text>\ls</xsl:text>
        <xsl:value-of select="@list-id"/>
        <xsl:text>\listid</xsl:text>
        <xsl:value-of select="@list-table-id"/>
        <xsl:text>}</xsl:text>
    </xsl:template>

    <xsl:template match="rtf:paragraph-style-in-table">
        <xsl:text>&#xA;{\s</xsl:text>
        <xsl:value-of select="@num"/>

        <xsl:call-template name="paragraph-info"/>
        <xsl:call-template name="character-info"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="@name"/>
        <xsl:text>;}&#xA;</xsl:text>
    </xsl:template>

    <xsl:template match="rtf:character-style-in-table">
        <xsl:text>&#xA;{\*\cs</xsl:text>
        <xsl:value-of select="@num"/>

        <xsl:call-template name="character-info"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="@name"/>
        <xsl:text>;}&#xA;</xsl:text>

    </xsl:template>


    <xsl:template name = "convert-level-number-type">

        <xsl:choose>
            <xsl:when test = "@numbering-type = 'Arabic' 
                or @numbering-type = 'arabic' 
                or @numbering-type = 'decimal'">
                    <xsl:text>\levelnfc0\levelnfcn0 </xsl:text>
            </xsl:when>
            <xsl:when test = "@numbering-type = 'uppercase Roman numeral' 
                    or @numbering-type = 'upper-roman'">
                <xsl:text>\levelnfc1\levelnfcn1 </xsl:text>
            </xsl:when> 
            <xsl:when test = "@numbering-type = 'lowercase Roman numeral' 
                or @numbering-type = 'lower-roman'">
                <xsl:text>\levelnfc2\levelnfcn2 </xsl:text>
            </xsl:when>
            <xsl:when test = "@numbering-type = 'uppercase letter' or
                @numbering-type = 'upper-alpha'">
                <xsl:text>\levelnfc3\levelnfcn3 </xsl:text>
            </xsl:when>
            <xsl:when test = "@numbering-type = 'lowercase letter' or
                @numbering-type = 'lower-alpha'">
                <xsl:text>\levelnfc4\levelnfcn4 </xsl:text>
            </xsl:when>
            <xsl:when test = "@numbering-type = 'bullet'">
                <xsl:text>\levelnfc22\levelnfcn22 </xsl:text>
            </xsl:when>
        </xsl:choose>

    </xsl:template>


</xsl:stylesheet>
