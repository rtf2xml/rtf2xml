<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:srtf="http://rtf2xml.sourceforge.net/simplertf"
    xmlns:rtf="http://rtf2xml.sourceforge.net/"
    exclude-result-prefixes="rtf srtf"
    version="1.0" 
    >
    <xsl:output 
    method = "xml"
    doctype-system = "/home/paul/cvs/rtf2xml/dtd/rtf2xml.53.dtd"
    indent= "yes"
    
    />
<xsl:template name = "convert-number">
    <xsl:param name = "number"/>
    <xsl:param name = "multiply-by">1</xsl:param>
    <xsl:choose>
        <xsl:when test = "contains($number, 'in')">
            <xsl:variable name = "temp-num" select = "substring-before($number, 'in')"/>
            <xsl:variable name = "temp-num2" select = "$temp-num * 72 "/>
            <xsl:variable name = "temp-num3">
                <xsl:call-template name = "round-100">
                    <xsl:with-param name = "num" select = "$temp-num2"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test = "$temp-num3 = 'NaN'">
                    <xsl:text>0</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select = "$temp-num3"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:when>
        <xsl:when test = "contains($number, 'mm')">
            <xsl:variable name = "temp-num" select = "substring-before($number, 'mm')"/>
            <xsl:variable name = "temp-num2" select = "$temp-num * 2.8346457 "/>
            <xsl:variable name = "temp-num3">
                <xsl:call-template name = "round-100">
                    <xsl:with-param name = "num" select = "$temp-num2"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test = "$temp-num3 = 'NaN'">
                    <xsl:text>0</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select = "$temp-num3"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:when>
        <xsl:when test = "contains($number, 'cm')">
            <xsl:variable name = "temp-num" select = "substring-before($number, 'cm')"/>
            <xsl:variable name = "temp-num2" select = "$temp-num * 28.346457 "/>
            <xsl:variable name = "temp-num3">
                <xsl:call-template name = "round-100">
                    <xsl:with-param name = "num" select = "$temp-num2"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test = "$temp-num3 = 'NaN'">
                    <xsl:text>0</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select = "$temp-num3"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:when>
        <xsl:when test = "contains($number, 'pc')">
            <xsl:variable name = "temp-num" select = "substring-before($number, 'pc')"/>
            <xsl:variable name = "temp-num2" select = "$temp-num div 12 "/>
            <xsl:variable name = "temp-num3">
                <xsl:call-template name = "round-100">
                    <xsl:with-param name = "num" select = "$temp-num2"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test = "$temp-num3 = 'NaN'">
                    <xsl:text>0</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select = "$temp-num3"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:when>


        <xsl:when test = "contains($number, 'pt')">
            <xsl:variable name = "temp-num" select = "substring-before($number, 'pt')"/>
            <xsl:variable name = "temp-num2" select = "$temp-num "/>
            <xsl:variable name = "temp-num3">
                <xsl:call-template name = "round-100">
                    <xsl:with-param name = "num" select = "$temp-num2"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test = "$temp-num3 = 'NaN'">
                    <xsl:text>0</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select = "$temp-num3"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
            <xsl:variable name = "temp-num">
                <xsl:value-of select = "round($number * $multiply-by)"/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test = "$temp-num = 'NaN'">
                    <xsl:text>0</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select = "$temp-num"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>




</xsl:stylesheet>
