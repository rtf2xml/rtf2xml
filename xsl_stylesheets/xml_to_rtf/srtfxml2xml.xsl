<!--

#########################################################################
#                                                                       #
#                                                                       #
#   copyright 2002 Paul Henry Tremblay                                  #
#                                                                       #
#   This program is distributed in the hope that it will be useful,     #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of      #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU    #
#   General Public License for more details.                            #
#                                                                       #
#   You should have received a copy of the GNU General Public License   #
#   along with this program; if not, write to the Free Software         #
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA            #
#   02111-1307 USA                                                      #
#                                                                       #
#                                                                       #
#########################################################################

-->


<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:srtf="http://rtf2xml.sourceforge.net/simplertf"
    xmlns:rtf="http://rtf2xml.sourceforge.net/"
    exclude-result-prefixes="rtf srtf"
    version="1.0" 
    >
    <xsl:output 
    method = "xml"
    doctype-system = "/home/paul/cvs/rtf2xml/dtd/rtf2xml.53.dtd"
    indent= "yes"
    
    />


	<doc:stylesheet xmlns:doc="http://www.kielen.com/documentation">
		<doc:name>xml2rtf.xsl</doc:name>
		<doc:used-by/>
		<doc:uses/>
                <doc:parameters>
                    <doc:parameter name = "default-list-indent">Determines how much lists are indented if not explicitly defined in the XML document.</doc:parameter>
                    
                </doc:parameters>
		<doc:entities/>
		<doc:description>
                    <doc:para>Author: Paul Tremblay</doc:para>
		   <doc:para>This stylesheet converts XML conforming to "" to RTF.</doc:para>
		</doc:description>
	</doc:stylesheet>

<xsl:include href = "srtfxml2xml_lists.xsl"/>
<xsl:include href = "paragraph_definition.xsl"/>
<xsl:include href = "../common/convert_number.xsl"/>


<xsl:param name = "default-list-indent">20</xsl:param>
<xsl:param name = "default-list-first-indent">15</xsl:param>
<!--
What is this para-style doing? ther is not element para!

-->
<xsl:key name="para-style" match="para" use="@name" />
<xsl:key name="color" match="*[@color]" use="@color" />
<xsl:key name="list" match="list-in-table" use="@id" />
<xsl:key name="font" match="*[@font-family]" use="@font-family" />
<xsl:key name="face" match="srtf:font" use="@face" />
    
<xsl:template match = "srtf:doc">
    <doc xmlns = "http://rtf2xml.sourceforge.net/">
        <xsl:apply-templates/>
    </doc>
    
</xsl:template>

<xsl:template match = "srtf:preamble|srtf:head">
    <preamble>
        <xsl:call-template name = "make-rtf-definition"/>
        <xsl:call-template name = "make-font-table"/>
        <xsl:call-template name = "make-color-table"/>
        <xsl:apply-templates select = "srtf:styles"/>
        <xsl:call-template name = "make-list-table"/>
        <!--
        <xsl:call-template name = "make-para-list-ids"/>
        -->
        <xsl:call-template name = "make-override-table"/>
        <xsl:call-template name = "make-page-definition"/>
    </preamble>
</xsl:template>


<xsl:template name = "make-font-table">
    <font-table>
        <xsl:for-each select = "/srtf:doc/descendant::*[generate-id(.)=generate-id(key('font', @font-family))]/@font-family">
            <xsl:call-template name = "font">
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select = "/srtf:doc/descendant::srtf:font[generate-id(.)=generate-id(key('face', @face))]/@face">
            <xsl:call-template name = "font">
            </xsl:call-template>
        </xsl:for-each>
    </font-table>
</xsl:template>

<xsl:template name = "font">
    <xsl:element name = "font-in-table">
    <xsl:attribute name = "name">
    <xsl:value-of select = "."/>
    </xsl:attribute>
    </xsl:element>
</xsl:template>


<xsl:template name = "make-color-table">
    <color-table>
        <xsl:for-each select = "/srtf:doc/descendant::*[generate-id(.)=generate-id(key('color', @color))]/@color">
            <xsl:call-template name = "color">
            </xsl:call-template>
        </xsl:for-each>
    </color-table>
</xsl:template>

<xsl:template name = "color">
    <xsl:element name = "color-in-table">
    <xsl:attribute name = "value">
    <xsl:value-of select = "."
                />
    </xsl:attribute>
    </xsl:element>
</xsl:template>

<xsl:template name = "make-rtf-definition">
    <xsl:element name = "rtf-definition">
        <xsl:attribute name = "code-page">
            <xsl:choose>
                <xsl:when test = "srtf:doc-defaults/@encoding">
                    <xsl:variable name = "encoding" select = "srtf:doc-defaults/@encoding"/>
                    <xsl:choose> 
                        <xsl:when test = "$encoding = 'latin1' or $encoding = 'latin-1'">
                            <xsl:text>ansicpg1252</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>ansicpg1252</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when> 
            </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name = "platform">
            <xsl:choose>
                <xsl:when test = "srtf:doc-defaults/@platform">
                    <xsl:value-of select = "srtf:doc-defaults/@platform"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>Windows</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name = "default-font">
            <xsl:choose>
                <xsl:when test = "srtf:doc-defaults/@font-family">
                    <xsl:value-of select = "srtf:doc-defaults/@font-family"/>
                </xsl:when>
                <xsl:when test = "srtf:doc/srtf:body/@font-family">
                    <xsl:value-of select = "srtf:doc/srtf:body/@font-family"/>
                </xsl:when>
            </xsl:choose>
            
        </xsl:attribute>
    </xsl:element>
</xsl:template>


<xsl:template match = "srtf:styles">
    <style-table>
        <paragraph-styles>
            <xsl:apply-templates select = "srtf:paragraph-style"/>
            <xsl:element name = "paragraph-style-in-table">
                <xsl:attribute name = "name">
                    <xsl:text>xml2rtf-default</xsl:text>
                </xsl:attribute>
                <xsl:attribute name = "num">
                    <xsl:value-of select = "count(srtf:paragraph-style) + 1"/>
                </xsl:attribute> 
            </xsl:element>
        </paragraph-styles>
        <character-styles>
            <xsl:apply-templates select = "srtf:character-style"/>
        </character-styles>
        <styles-in-body>
            <paragraph-style-in-body name="xml2rtf-default" style-number="s-1" />
        </styles-in-body>
    </style-table>
</xsl:template>

<xsl:template match = "srtf:paragraph-style">
    <xsl:element name = "paragraph-style-in-table">
        <xsl:attribute name = "num">
            <xsl:number/>
        </xsl:attribute>
        <xsl:call-template name = "get-block-styles"/>
    </xsl:element>
</xsl:template>

<xsl:template name = "get-block-styles">
    <xsl:if test = "@name">
        <xsl:attribute name = "name">
            <xsl:value-of select = "@name"/>
        </xsl:attribute>
    </xsl:if>
    <xsl:choose>
        <xsl:when test = "@space-after">
           <xsl:attribute name = "space-after">
               <xsl:value-of select = "@space-after"/>
           </xsl:attribute> 
        </xsl:when>
    </xsl:choose>
    
</xsl:template>



<xsl:template name = "make-override-table">
</xsl:template>

<xsl:template name = "make-page-definition">
    <xsl:element name = "page-definition">
    <xsl:attribute name = "margin-right">
        <xsl:choose>
            <xsl:when test = "srtf:doc-defaults/@margin-right">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "srtf:doc-defaults/@margin-right"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>90</xsl:otherwise>
        </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name = "margin-left">
        <xsl:choose>
            <xsl:when test = "srtf:doc-defaults/@margin-left">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "srtf:doc-defaults/@margin-left"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>90</xsl:otherwise>
        </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name = "margin-top">
        <xsl:choose>
            <xsl:when test = "srtf:doc-defaults/@margin-top">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "srtf:doc-defaults/@margin-top"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>72</xsl:otherwise>
        </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name = "margin-bottom">
        <xsl:choose>
            <xsl:when test = "srtf:doc-defaults/@margin-bottom">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "srtf:doc-defaults/@margin-bottom"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>72</xsl:otherwise>
        </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name = "gutter">
        <xsl:choose>
            <xsl:when test = "srtf:doc-defaults/@gutter">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "@gutter"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
    </xsl:attribute>
    </xsl:element>
</xsl:template>

<xsl:template match = "srtf:body">
    <xsl:choose>
        <xsl:when test = "srtf:section">
            <body>
                <xsl:apply-templates/>
            </body>
        </xsl:when>
        <xsl:otherwise>
            <body>
               <section type = "rtf-native" num = "1" level = "0" num-in-level = "1">
                    <xsl:apply-templates/>
               </section> 
            </body>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match = "srtf:body/srtf:section">
    <xsl:variable name = "count">
        <xsl:number/>
    </xsl:variable>
    <xsl:element name = "section">
        <xsl:attribute name = "type">
            <xsl:text>rtf-native</xsl:text>
        </xsl:attribute>
        <xsl:attribute name = "num">
            <xsl:value-of select = "$count"/>
        </xsl:attribute>
        <xsl:attribute name = "num-in-level">
            <xsl:value-of select = "$count"/>
        </xsl:attribute>
        <xsl:attribute name = "level">
            <xsl:text>0</xsl:text>
        </xsl:attribute>
        <xsl:apply-templates/>
    </xsl:element>
    
</xsl:template>





<xsl:template match = "srtf:p">
    <xsl:element name = "paragraph-definition">
    <xsl:call-template name = "paragraph-definition"/>
    <para>
        <xsl:apply-templates/>
    </para>
    </xsl:element>
</xsl:template>

<xsl:template match = "srtf:li/srtf:p[1]" priority = "5">
    <xsl:element name = "paragraph-definition">
        <xsl:call-template name = "list-left-indent">
            <xsl:with-param name = "level" select = "count(ancestor::srtf:ol|ancestor::srtf:ul) "/>
        </xsl:call-template>
        <xsl:call-template name = "paragraph-definition">
            <xsl:with-param name = "list">true</xsl:with-param>
        </xsl:call-template>
        <para>
            <xsl:apply-templates/>
        </para>
    </xsl:element>
</xsl:template>

<xsl:template match = "srtf:li/srtf:p" priority = "4">
    <xsl:element name = "paragraph-definition">
        <xsl:call-template name = "list-left-indent">
            <xsl:with-param name = "level" select = "count(ancestor::srtf:ol|ancestor::srtf:ul) "/>
            <xsl:with-param name = "do-first-line">false</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name = "paragraph-definition">
            <xsl:with-param name = "list">true</xsl:with-param>
        </xsl:call-template>
        <para>
            <xsl:apply-templates/>
        </para>
    </xsl:element>
</xsl:template>



<xsl:template match = "srtf:head/text()|srtf:body/text()|srtf:ol/text()|srtf:ul/text()|srtf:section/text()|srtf:div/text()|srtf:styles/text()|srtf:p-style-define/text()|c-style-define/text()|srtf:doc-defaults/text()|srtf:doc/text()|srtf:border/text()|srtf:li/text()">

    <xsl:value-of select = "normalize-space(.)"/>

</xsl:template>


<xsl:template name = "round-100">
    <xsl:param name = "num"/>
    <xsl:variable name = "temp-num" select = "round($num * 100)"/>
    <xsl:value-of select = "$temp-num div 100"/>
</xsl:template>

</xsl:stylesheet>
