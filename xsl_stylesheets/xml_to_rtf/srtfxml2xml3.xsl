<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:rtf="http://rtf2xml.sourceforge.net/"
    exclude-result-prefixes="rtf"
    version="1.0" 
    >
    <xsl:output 
    method = "xml"
    doctype-system = "/home/paul/cvs/rtf2xml/dtd/rtf2xml.53.dtd"
    
    />

<xsl:key name="list" match="rtf:list-in-table" use="@list-table-id" />
<xsl:key name="override" match="rtf:override-list" use="@list-id" />

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>


    <xsl:template match = "rtf:item">
        <xsl:element name = "item">
            <xsl:variable name = "id" select = "../@list-id"/>
            <xsl:variable name = "level" select = "../@level"/>
            <xsl:call-template name = "make-number">
                <xsl:with-param name = "id" select = "$id"/>
                <xsl:with-param name = "level" select = "$level"/>
            </xsl:call-template>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template name = "make-number">
        <xsl:param name = "id"/>
        <xsl:param name = "level"/>
        <xsl:choose>
            <xsl:when test = "child::rtf:list[1]">
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name = "insert-number">
                    <xsl:with-param name = "level" select = "$level"/>
                    <xsl:with-param name = "id" select = "$id"/>
                </xsl:call-template>
                
                <xsl:attribute name = "level">
                    <xsl:value-of select = "$level"/>
                </xsl:attribute>
                <xsl:attribute name = "id">
                    <xsl:value-of select = "$id"/>
                </xsl:attribute>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name = "insert-number_"/>

    <xsl:template name = "insert-number">
        <xsl:param name = "level"/>
        <xsl:param name = "id"/>
        <xsl:variable name = "list-table-id">
            <xsl:value-of select = "key('override', $id)/@list-table-id"/>
        </xsl:variable>
        <xsl:variable name = "number-type">
            <xsl:value-of select = "key('list', $list-table-id)/rtf:level-in-table[@level = $level]/@numbering-type"/>
        </xsl:variable>
        <xsl:attribute name = "number">
            <xsl:choose>
                <xsl:when test = "$number-type = 'arabic'or $number-type = 'Arabic' or $number-type = 'decimal'">
                    <xsl:number/>
                </xsl:when>
                <xsl:when test = "$number-type = 'upper-alpha'">
                    <xsl:number format = "A"/>
                </xsl:when>
                <xsl:when test = "$number-type = 'lower-alpha'">
                    <xsl:number format = "a"/>
                </xsl:when>
                <xsl:when test = "$number-type = 'upper-roman'">
                    <xsl:number format = "I"/>
                </xsl:when>
                <xsl:when test = "$number-type = 'lower-roman'">
                    <xsl:number format = "i"/>
                </xsl:when>
                <xsl:when test = "$number-type = 'bullet'">
                    <xsl:text>&#xB7;</xsl:text>
                </xsl:when>
            </xsl:choose>
        </xsl:attribute>
        
    </xsl:template>



</xsl:stylesheet>
