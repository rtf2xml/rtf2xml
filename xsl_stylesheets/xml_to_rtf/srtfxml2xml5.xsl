<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:rtf="http://rtf2xml.sourceforge.net/"
    exclude-result-prefixes="rtf"
    version="1.0" 
    >
    <xsl:output 
    method = "xml"
    doctype-system = "/home/paul/cvs/rtf2xml/dtd/rtf2xml.53.dtd"
    
    />


    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match = "rtf:list">
        <xsl:choose>
            <xsl:when test = "ancestor::rtf:list">
                <xsl:choose>
                    <xsl:when test = "not(parent::rtf:item)">
                        <xsl:element name = "item">
                            <xsl:element name = "list">
                                <xsl:copy-of select = "@*"/>
                                <xsl:apply-templates/>
                            </xsl:element>
                        </xsl:element>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name = "list">
                            <xsl:copy-of select = "@*"/>
                            <xsl:apply-templates/>
                        </xsl:element>
                        
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name = "list">
                    <xsl:copy-of select = "@*"/>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match = "rtf:item">
        <xsl:element name = "item">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template match = "rtf:item//rtf:paragraph-definition[1]">
        <xsl:element name = "paragraph-definition">
            <xsl:attribute name = "list-id">
                <xsl:value-of select = "ancestor::rtf:item/@id"/>
            </xsl:attribute>
            <xsl:attribute name = "list-level">
                <xsl:value-of select = "ancestor::rtf:item/@level - 1"/>
            </xsl:attribute>
                
            <xsl:copy-of select = "@*"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template match = "rtf:item//rtf:para[1]">
        <xsl:element name = "para">
            <xsl:element name = "list-text">
                <xsl:value-of select = "ancestor::rtf:item/@final-number"/>
                <xsl:text>&#x009;</xsl:text>
            </xsl:element>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>




</xsl:stylesheet>
