<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY copyright "Copyright &#x00A9; 2004">
    <!ENTITY namespace "http://rtf2xml.sourceforge.net/">
    <!ENTITY current-dtd "http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd">
]><!--
#########################################################################
#                                                                       #
#                                                                       #
#   copyright 2002 Paul Henry Tremblay                                  #
#                                                                       #
#   This program is distributed in the hope that it will be useful,     #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of      #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU    #
#   General Public License for more details.                            #
#                                                                       #
#   You should have received a copy of the GNU General Public License   #
#   along with this program; if not, write to the Free Software         #
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA            #
#   02111-1307 USA                                                      #
#                                                                       #
#                                                                       #
#########################################################################
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://www.kielen.com/documentation" exclude-result-prefixes="rtf doc" xmlns:rtf="http://rtf2xml.sourceforge.net/">


    <doc:stylesheet>
        <doc:name>header.xsl</doc:name>
        <doc:used-by>
            <doc:stylesheet-name>to_sdoc.xsl</doc:stylesheet-name>
        </doc:used-by>
        <doc:uses/>
        <doc:parameters/>
        <doc:entities>
            <doc:entity name="copyright">Copyright &#x00A9; 2004</doc:entity>
            <doc:entity name="namespace">http://rtf2xml.sourceforge.net/</doc:entity>
            <doc:entity name="current-dtd">http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd</doc:entity>
            <doc:entity name="tei-dtd">http://rtf2xml.sourceforge.net/dtd/teilite.dtd</doc:entity>
        </doc:entities>
        <doc:description>
            <doc:para>&copyright; by Paul Tremblay</doc:para>
            <doc:para>The namespace "rtf" is mapped to "&namespace;".</doc:para>
            <doc:para>This stylesheet creates a TEI.2 header.</doc:para>
        </doc:description>
    </doc:stylesheet>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:preamble</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>Delete all information in this element.</doc:para>
        </doc:description>
    </doc:template>

    <xsl:template match="rtf:preamble"/>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>make-header</doc:name>
        <doc:type>name</doc:type>
        <doc:parameters/>
        <doc:called-by>
            <doc:template-name>to_tei.xsl#/</doc:template-name>
        </doc:called-by>
        <doc:description>

            <doc:para>If the element /rtf:doc/rtf:preamble/rtf:doc-information exists, then 
            create &lt;articleinfo&gt; element. 
            </doc:para>

            <doc:para>
                Otherwise, do nothing.
            </doc:para>

        </doc:description>
    </doc:template>
    <xsl:template name="make-header">
        <xsl:if test = "/rtf:doc/rtf:preamble/rtf:doc-information">
            <articleinfo>
                <xsl:apply-templates select="/rtf:doc/rtf:preamble/rtf:doc-information" mode="header"/>
            </articleinfo>
        </xsl:if>
    </xsl:template>

    <doc:template mode="header" xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:doc-information</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>Use the rtf:doc-information to build the necessary elements for docbook.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match="rtf:doc-information" mode="header">
        <xsl:choose>
            <xsl:when test="rtf:title">
                <xsl:apply-templates select="rtf:title" mode="header"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="title">
                    <xsl:text>No Title</xsl:text>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="rtf:author">
                <xsl:apply-templates select="rtf:author" mode="header"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="author">
                    <xsl:text>No Author</xsl:text>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:call-template name="revision"/>
    </xsl:template>


    <doc:template mode="header" xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:title</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>Use the rtf:title to build the title element.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match="rtf:title" mode="header">
        <title>
            <xsl:apply-templates/>
        </title>
    </xsl:template>


    <doc:template mode="header" xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:author</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>Use the rtf:author to build the author element.</doc:para>
        </doc:description>
    </doc:template>

    <xsl:template match="rtf:author" mode="header">
        <author>
            <firstname>
                <xsl:value-of select="substring-before(., ' ')"/>
            </firstname>
            <surname>
                <xsl:value-of select="substring-after(., ' ')"/>
            </surname>
        </author>
    </xsl:template>



    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>revision</doc:name>
        <doc:type>name</doc:type>
        <doc:parameters/>
        <doc:called-by>
            <doc:template-name>#rtf:doc-information</doc:template-name>
        </doc:called-by>
        <doc:description>
            <doc:para>Creates revision element based on information found in the element creation-time.</doc:para>
        </doc:description>
    </doc:template>

    <xsl:template name="revision">
        <revhistory>
            <revision>
                <revnumber>1.0</revnumber>
                    <date>
                        <xsl:variable name="year" select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:creation-time/@year"/>
                        <xsl:variable name="month" select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:creation-time/@month"/>
                        <xsl:variable name="day" select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:creation-time/@day"/>
                        <xsl:value-of select="$year"/>
                        <xsl:text>-</xsl:text>
                        <xsl:if test="string-length($month) = 1">
                            <xsl:text>0</xsl:text>
                        </xsl:if>
                        <xsl:value-of select="$month"/>
                        <xsl:text>-</xsl:text>
                        <xsl:if test="string-length($day) = 1">
                            <xsl:text>0</xsl:text>
                        </xsl:if>
                        <xsl:value-of select="$day"/>
                    </date>
                <revremark>First started document.</revremark>
            </revision>
            <revision>
                <revnumber>1.0</revnumber>
                <date>
                    <xsl:variable name="year" select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:revision-time/@year"/>
                    <xsl:variable name="month" select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:revision-time/@month"/>
                    <xsl:variable name="day" select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:revision-time/@day"/>
                    <xsl:value-of select="$year"/>
                    <xsl:text>-</xsl:text>
                    <xsl:if test="string-length($month) = 1">
                        <xsl:text>0</xsl:text>
                    </xsl:if>
                    <xsl:value-of select="$month"/>
                    <xsl:text>-</xsl:text>
                    <xsl:if test="string-length($day) = 1">
                        <xsl:text>0</xsl:text>
                    </xsl:if>
                    <xsl:value-of select="$day"/>
                </date>
                <revremark>Last updated document.</revremark>
            </revision>
        </revhistory>
    </xsl:template>



</xsl:stylesheet>
