<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY copyright "Copyright &#x00A9; 2004">
    <!ENTITY namespace "http://rtf2xml.sourceforge.net/">
    <!ENTITY current-dtd "http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd">
]><!--
#########################################################################
#                                                                       #
#                                                                       #
#   copyright 2002 Paul Henry Tremblay                                  #
#                                                                       #
#   This program is distributed in the hope that it will be useful,     #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of      #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU    #
#   General Public License for more details.                            #
#                                                                       #
#   You should have received a copy of the GNU General Public License   #
#   along with this program; if not, write to the Free Software         #
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA            #
#   02111-1307 USA                                                      #
#                                                                       #
#                                                                       #
#########################################################################
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://www.kielen.com/documentation" exclude-result-prefixes="rtf doc" xmlns:rtf="http://rtf2xml.sourceforge.net/">


    <doc:stylesheet>
        <doc:name>inline.xsl</doc:name>
        <doc:used-by>
            <doc:stylesheet-name>to_sdoc.xsl</doc:stylesheet-name>
        </doc:used-by>
        <doc:uses/>
        <doc:parameters/>
        <doc:entities>
            <doc:entity name="copyright">Copyright &#x00A9; 2004</doc:entity>
            <doc:entity name="namespace">http://rtf2xml.sourceforge.net/</doc:entity>
            <doc:entity name="current-dtd">http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd</doc:entity>
        </doc:entities>
        <doc:description>
            <doc:para>&copyright; by Paul Tremblay</doc:para>
            <doc:para>The namespace "rtf" is mapped to "&namespace;".</doc:para>
            <doc:para>This stylesheet handles all elments withing the &lt;p&gt; element.</doc:para>
        </doc:description>
    </doc:stylesheet>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>inline(character-style)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with a character-style 
            attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@character-style]" priority="15">
        <xsl:element name = "emphasis">
            <xsl:attribute name = "role">
                <xsl:value-of select = "@character-style"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(footnote-marker)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Delete footnote-markers.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@footnote-marker]" priority="11"/>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(hidden)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Delete hidden text.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@hidden='true']" priority="11"/>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(superscript)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            a attribute of superscript.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@superscript]" priority="11">
          <emphasis role = "superscript">
              <xsl:apply-templates/>
          </emphasis>
    </xsl:template>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(subscript)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            an attribute of subscript.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@subscript]" priority="11">
          <emphasis role = "subscript">
              <xsl:apply-templates/>
          </emphasis>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(annotation)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>For elements that match inline annotation.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@annotation='true']" priority="10">
        <xsl:choose>
            <xsl:when test = "$delete-annotation='true'"/>
            <xsl:otherwise>
                <note role = "annotation">
                    <xsl:apply-templates/>
                </note>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(italics-bold-underlined)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with a italics, 
            bold, and underlined attributes.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@italics][@bold][@underlined]" priority="8">
        <xsl:choose>
          <xsl:when test = "@italics = 'true'">
              <emphasis role = "italics-bold-underlined">
                  <xsl:apply-templates/>
              </emphasis>
          </xsl:when>
          <xsl:otherwise>
              <xsl:if test = "../../@italics='true'">
                  <emphasis role = "paragraph-emphasis-off">
                      <xsl:apply-templates/>
                  </emphasis>
              </xsl:if>
          </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(italics-bold)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with a italics 
            and bold attributes.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@italics][@bold]" priority="6">
        <xsl:choose>
          <xsl:when test = "@italics = 'true'">
              <emphasis role = "italics-bold">
                  <xsl:apply-templates/>
              </emphasis>
          </xsl:when>
          <xsl:otherwise>
              <xsl:if test = "../../@italics='true'">
                  <emphasis role = "paragraph-emphasis-off">
                      <xsl:apply-templates/>
                  </emphasis>
              </xsl:if>
          </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(italics-underlined)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            italics and underlined attributes.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@italics][@underlined]" priority="6">
        <xsl:choose>
          <xsl:when test = "@italics = 'true'">
              <emphasis role = "italics-underlined">
                  <xsl:apply-templates/>
              </emphasis>
          </xsl:when>
          <xsl:otherwise>
              <xsl:if test = "../../@italics='true'">
                  <emphasis role = "paragraph-emphasis-off">
                      <xsl:apply-templates/>
                  </emphasis>
              </xsl:if>
          </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(bold-underlined)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            bold and underlined attributes.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@bold][@underlined]" priority="6">
        <xsl:choose>
          <xsl:when test = "@italics = 'true'">
              <emphasis role = "bold-underlined">
                  <xsl:apply-templates/>
              </emphasis>
          </xsl:when>
          <xsl:otherwise>
              <xsl:if test = "../../@italics='true'">
                  <emphasis role = "paragraph-emphasis-off">
                      <xsl:apply-templates/>
                  </emphasis>
              </xsl:if>
          </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>inline(italics)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with a italics 
            attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@italics]" priority="4">
        <xsl:choose>
          <xsl:when test = "@italics = 'true'">
              <emphasis role = "italics">
                  <xsl:apply-templates/>
              </emphasis>
          </xsl:when>
          <xsl:otherwise>
              <xsl:if test = "../../@italics='true'">
                  <emphasis role = "paragraph-emphasis-off">
                      <xsl:apply-templates/>
                  </emphasis>
              </xsl:if>
          </xsl:otherwise>
        </xsl:choose>
    </xsl:template>



    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:bold</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            a boldattribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@bold]" priority="4">
        <xsl:choose>
          <xsl:when test = "@bold = 'true'">
              <emphasis role = "bold">
                  <xsl:apply-templates/>
              </emphasis>
          </xsl:when>
          <xsl:otherwise>
              <xsl:if test = "../../@bold='true'">
                  <emphasis role = "paragraph-emphasis-off">
                      <xsl:apply-templates/>
                  </emphasis>
              </xsl:if>
          </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(underlined)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            a boldattribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@underlined]" priority="4">
        <xsl:choose>
          <xsl:when test = "@underlined = 'false'">
              <xsl:if test = "../../@underlined='true'">
                  <emphasis role = "paragraph-emphasis-off">
                      <xsl:apply-templates/>
                  </emphasis>
              </xsl:if>
          </xsl:when>
          <xsl:otherwise>
              <emphasis role = "underlined">
                  <xsl:apply-templates/>
              </emphasis>
          </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(font-size)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            a font-size attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@font-size]" priority="2">
        <xsl:element name = "emphasis">
            <xsl:attribute name = "role">
                <xsl:text>font-size:</xsl:text>
                <xsl:value-of select = "@font-size"/>
            </xsl:attribute>
        </xsl:element>
        <xsl:apply-templates/>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(font-style)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            a font-style attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@font-style]" priority="2">
        <xsl:element name = "emphasis">
            <xsl:attribute name = "role">
                <xsl:text>font-style:</xsl:text>
                <xsl:value-of select = "@font-style"/>
            </xsl:attribute>
        </xsl:element>
        <xsl:apply-templates/>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(font-color)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            a font-color attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@font-color]" priority="2">
        <xsl:element name = "emphasis">
            <xsl:attribute name = "role">
                <xsl:text>font-color:</xsl:text>
                <xsl:value-of select = "@font-color"/>
            </xsl:attribute>
        </xsl:element>
        <xsl:apply-templates/>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(strike-through)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            a strike-through attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@strike-through='true']" priority="1">
          <emphasis role = "strike-through">
              <xsl:apply-templates/>
          </emphasis>
    </xsl:template>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(double-strike-through)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            an attribute of double-strike-through attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@double-strike-through='true']" priority="1">
          <emphasis role = "double-strike-through">
              <xsl:apply-templates/>
          </emphasis>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(small-caps)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            a small-caps attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@small-caps]">
        <xsl:choose>
          <xsl:when test = "@small-caps = 'true'">
              <emphasis role = "small-caps">
                  <xsl:apply-templates/>
              </emphasis>
          </xsl:when>
          <xsl:otherwise>
              <xsl:if test = "../../@small-caps='true'">
                  <emphasis role = "paragraph-emphasis-off">
                      <xsl:apply-templates/>
                  </emphasis>
              </xsl:if>
          </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(shadow)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            an shadow attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@shadow]">
          <emphasis role = "shadow">
              <xsl:apply-templates/>
          </emphasis>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(outline)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            a outline attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@outline]">
          <emphasis role = "outline">
              <xsl:apply-templates/>
          </emphasis>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(emboss)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            an emboss attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@emboss]">
          <emphasis role = "emboss">
              <xsl:apply-templates/>
          </emphasis>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(engrave)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            a engrave attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@engrave]">
          <emphasis role = "engrave">
              <xsl:apply-templates/>
          </emphasis>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:inline(caps)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;inline&gt; element with  
            a caps attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:inline[@caps]">
        <xsl:apply-templates/>
    </xsl:template>


    

</xsl:stylesheet>
