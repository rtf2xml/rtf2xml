#!/usr/bin/env python
import os, sys

class Format:

    def __init__(self):
        self.__target_dir = 'docs'

    def make_the_docs(self, the_files):
        for the_file in the_files:
            if not os.path.isfile(the_file):
                continue
            filename, ext = os.path.splitext(the_file)
            if ext != '.xsl':
                continue
            out_file = '%s.htm' % filename
            target = os.path.join(self.__target_dir, out_file)
            command = 'xsl_format.sh %s > %s' % (the_file, target)
            os.system(command)


if __name__ == '__main__':
    all_files = sys.argv[1:]
    format_obj = Format()
    format_obj.make_the_docs(all_files)
    sys.stdout.write('Script finished\n')


