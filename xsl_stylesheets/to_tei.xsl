<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:rtf="http://rtf2xml.sourceforge.net/"
    version="1.0"
    exclude-result-prefixes="rtf "
        >


    <xsl:output 
    method = "xml" 
    doctype-system = "http://rtf2xml.sourceforge.net/dtd/teilite.dtd"
    doctype-public = ""
    />


    <!--
    <xsl:output
    method = "xml"
    doctype-system = "/home/paul/cvs/rtf2xml/dtd/teilite.dtd"
    />

    -->
    <!--
    <xsl:output 
    method = "xml" 
    doctype-system = "http://rtf2xml.sourceforge.net/dtd/teilite.dtd"
    doctype-public = ""
    />
    -->


    <!--
http://www.tei-c.org/TEI/Lite/DTD/

    -->
    <xsl:param name = "keep-list-text">false</xsl:param>

    <!--Get rid of toc, index, toa-->
    <xsl:param name = "keep-index">true</xsl:param>

    <xsl:template match = "rtf:doc">
        <TEI.2>
            <xsl:apply-templates select = "rtf:preamble"/>
            <xsl:element name = "text">
               <xsl:element name = "body">
                    <xsl:apply-templates select = "rtf:body"/>
               </xsl:element> 
            </xsl:element>
        </TEI.2>
        </xsl:template>

    <xsl:template match = "rtf:preamble">
            <xsl:choose>
                <xsl:when test = "rtf:doc-information">
                    <teiHeader>
                        <xsl:apply-templates select = "rtf:doc-information"/>
                    </teiHeader>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name = "tei-head"/>
                </xsl:otherwise>
            </xsl:choose>
    </xsl:template>

    <xsl:template name = "pub-state">
        <publicationStmt>
            <distributor>
                <xsl:text>none</xsl:text>
            </distributor>
        </publicationStmt>
        <sourceDesc>
            <p>no source--a manuscript</p>
        </sourceDesc>
        
    </xsl:template>
    
    <xsl:template name = "tei-head">
        <teiHeader> 
            <fileDesc>
                <titleStmt>
                    <title>
                        <xsl:text>Manuscript</xsl:text>
                    </title>
                </titleStmt>
                <publicationStmt>
                    <distributor>
                        <xsl:text>none</xsl:text>
                    </distributor>
                </publicationStmt>
                <sourceDesc>
                    <p>no source--a manuscript</p>
                </sourceDesc>
            </fileDesc>
        </teiHeader>
    </xsl:template>

    <xsl:template match = "rtf:doc-information">
        <fileDesc>
            <titleStmt>
                <xsl:choose>
                    <xsl:when test = "rtf:title">
                        <xsl:apply-templates select = "rtf:title"/>
                    </xsl:when>
                    <xsl:otherwise>
                       <xsl:element name = "title">
                           <xsl:text>No Author</xsl:text>
                       </xsl:element> 
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:choose>
                   <xsl:when test = "rtf:author">
                        <xsl:apply-templates select = "rtf:author"/>
                   </xsl:when> 
                   <xsl:otherwise>
                       <xsl:element name = "title">
                          <xsl:text>No Title</xsl:text> 
                       </xsl:element>
                   </xsl:otherwise>
                </xsl:choose>
            </titleStmt>
            <xsl:call-template name = "pub-state"/>
        </fileDesc>
        <xsl:call-template name = "revision"/>
    </xsl:template>
            
    <xsl:template match = "rtf:title">
        <title>
            <xsl:apply-templates/>
        </title>
    </xsl:template>

    <xsl:template match = "rtf:author">
        <author>
            <xsl:apply-templates/>
        </author>
        
    </xsl:template>


    <xsl:template name = "resp-author">
        <xsl:choose>
            <xsl:when test = "descendant::rtf:author">
                <xsl:apply-templates select ="rtf:author" mode = "resp-author"/>
            </xsl:when>
        </xsl:choose>
        
    </xsl:template>



    <xsl:template match = "rtf:author" mode = "resp-author">
            <xsl:apply-templates/>
    </xsl:template>

    <xsl:template name = "revision">
        <revisionDesc  >
            <change >
            <date>
            <xsl:value-of select = "/rtf:doc/rtf:preamble/rtf:doc-information/rtf:creation-time/@day"/>
            <xsl:text> </xsl:text>
            <xsl:call-template name = "month">
                <xsl:with-param name = "num">
                    <xsl:value-of select = "/rtf:doc/rtf:preamble/rtf:doc-information/rtf:creation-time/@month"/>
                </xsl:with-param>
            </xsl:call-template>
            <xsl:text> </xsl:text>
            <xsl:value-of select = "/rtf:doc/rtf:preamble/rtf:doc-information/rtf:creation-time/@year"/>
            </date>
                <respStmt>
                    <resp>written by</resp>
                    <name>
                        <xsl:call-template name = "resp-author"/>
                    </name>
                </respStmt>
            <item>First started document.</item>
            </change>
         <change>
            <date>
                <xsl:value-of select = "/rtf:doc/rtf:preamble/rtf:doc-information/rtf:revision-time/@day"/>
                <xsl:text> </xsl:text>
                <xsl:call-template name = "month">
                    <xsl:with-param name = "num">
                        <xsl:value-of select = "/rtf:doc/rtf:preamble/rtf:doc-information/rtf:revision-time/@month"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:text> </xsl:text>
                <xsl:value-of select = "/rtf:doc/rtf:preamble/rtf:doc-information/rtf:revision-time/@year"/>
            </date>
                <respStmt>
                    <resp>written by</resp>
                    <name>
                        <xsl:call-template name = "resp-author"/>
                    </name>
                </respStmt>
                <item>Last updated document.</item>
         </change>
        </revisionDesc>
        
    </xsl:template>

    <xsl:template name = "month">
        <xsl:param name = "num"></xsl:param>
        <xsl:choose>
            <xsl:when test = "$num='1'">
                <xsl:text>January</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='2'">
                <xsl:text>February</xsl:text>
            </xsl:when>
            <xsl:when test = "$num = '3'">
                <xsl:text>March</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='4'">
                <xsl:text>April</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='5'">
                <xsl:text>May</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='6'">
                <xsl:text>June</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='7'">
                <xsl:text>July</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='8'">
                <xsl:text>August</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='9'">
                <xsl:text>September</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='10'">
                <xsl:text>October</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='11'">
                <xsl:text>November</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='12'">
                <xsl:text>December</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
            
    <xsl:template match = "rtf:para">
        <xsl:if test = "normalize-space(.)">
            <xsl:choose>
                <xsl:when test = "ancestor::rtf:cell">
                    <xsl:call-template name = "para-content"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:element name = "p">
                        <xsl:attribute name = "rend">
                            <xsl:value-of select = "../@name"/>
                        </xsl:attribute>
                        <xsl:call-template name = "para-content"/>
                    </xsl:element>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template match = "rtf:cell/rtf:para">
        <xsl:call-template name = "para-content"/>
    </xsl:template>

    <xsl:template name = "para-content">
        <xsl:choose>
            <xsl:when test = "@italics = 'true' ">
               <emph rend = "paragraph-emph-italics">
                    <xsl:apply-templates/>
               </emph> 
            </xsl:when>
            <xsl:when test = "@bold = 'true' ">
               <emph rend = "paragraph-emph-bold">
                    <xsl:apply-templates/>
               </emph> 
            </xsl:when>
            <xsl:when test = "@underlined">
               <emph rend = "paragraph-emph-underlined">
                    <xsl:apply-templates/>
               </emph> 
            </xsl:when>
            <xsl:when test = "(@strike-through = 'true')
                or (@double-strike-through = 'true')
                or (@emboss = 'true')
                or (@engrave = 'true')
                or (@small-caps = 'true')
                or (@shadow = 'true')
                or (@hidden = 'true')
                or (@outline = 'true')
            
                ">
               <emph rend = "paragraph-emph">
                    <xsl:apply-templates/>
               </emph> 
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>

    <!--TABLE-->

    <!--
    <xsl:template match = "table">
        <xsl:element name = "table">
            <title/>
            <xsl:element name = "tgroup">
                <xsl:attribute name = "cols">
                    <xsl:value-of select = "@number-of-columns"/>
                </xsl:attribute>
                <xsl:if test = "row[@header = 'true']">
                    <xsl:element name = "thead">
                        <xsl:apply-templates select = "row[@header = 'true']" mode = "header"/>
                    </xsl:element>
                </xsl:if>
                <xsl:element name = "tbody">
                    <xsl:apply-templates select = "not(row[@header = 'true'])"/>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    -->
    <xsl:template match = "rtf:table">
        <xsl:element name = "table">
                <xsl:attribute name = "cols">
                    <xsl:value-of select = "@number-of-columns"/>
                </xsl:attribute>
                <xsl:attribute name = "rows">
                    <xsl:value-of select = "@number-of-rows"/>
                </xsl:attribute>
                    <xsl:apply-templates/> 
        </xsl:element>
    </xsl:template>

    <xsl:template match = "rtf:row">
        <xsl:choose>
            <xsl:when test = "normalize-space(.)">
                <xsl:element name = "row">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
        </xsl:choose>
    </xsl:template>


    <xsl:template match = "rtf:row[@header = 'true']" mode = "header">
        <xsl:choose>
            <xsl:when test = "normalize-space(.)">
                <xsl:element name = "row">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match = "rtf:cell">
        <xsl:element name = "cell">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>


    <xsl:template match = "rtf:footnote">
        <xsl:element name = "note">
            <xsl:choose>
                <xsl:when test = "@type='endnote'">
                    <xsl:attribute name = "type">
                        <xsl:text>endnote</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name = "type">
                        <xsl:text>footnote</xsl:text>
                    </xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>




    <xsl:template match = "rtf:wrap-list">
        <xsl:choose>
            <xsl:when test = "descendant::rtf:list-text[@type='ordered']">
                <list type = "ordered">
                    <xsl:apply-templates/>
                </list>
            </xsl:when>
            <xsl:otherwise>
                <list type = "bulleted">
                    <xsl:apply-templates/>
                </list>
                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match = "rtf:wrap-item">
        <item>
            <xsl:apply-templates/>
        </item>
    </xsl:template>

    <xsl:template match = "list-text_">
        <xsl:choose>
            <xsl:when test = "$keep-list-text = 'true'">
                <xsl:apply-templates/>
            </xsl:when>
        </xsl:choose>
        
    </xsl:template>

    <xsl:template match = "rtf:list-text">
        <xsl:choose>
            <xsl:when test = "ancestor::rtf:wrap-list">
                
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    


    <xsl:template match = "rtf:section">
        <xsl:choose>
            <xsl:when test = "normalize-space(.)">
                <xsl:choose>
                    <xsl:when test = 
                        "rtf:field-block[@type = 'index']
                        or rtf:field-block[@type = 'toc']
                        or rtf:field-block[@type = 'toa']
                    ">
                        <xsl:if test = "$keep-index = 'true'">
                            <xsl:element name = "div">
                                <xsl:attribute name = "rend">
                                    <xsl:value-of select = "field-block/@type"/>
                                </xsl:attribute>
                                <xsl:apply-templates/>
                            </xsl:element>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                        <div>
                            <xsl:apply-templates/>
                        </div>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!--paragraph-definition to other elements-->
    <!--

    <xsl:template match = "paragraph-definition">
        <xsl:choose>
            <xsl:when test = "(@name = 'quote') or (@name = 'blockquote')">
                <xsl:element name = "blockquote">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    -->


    <xsl:template match = "rtf:wrap-paragraph-definition">
        <xsl:choose>
            <xsl:when test = "(@name = 'quote') 
                or (@name = 'blockquote') 
                or (@name = 'Blockquote')
                
                ">
                <xsl:element name = "quote">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--
    INLINE ITEMS
    -->
    <xsl:template match = "rtf:inline">
        <xsl:choose>
            <xsl:when test = "normalize-space(.)">
                <xsl:call-template name = "inline"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name = "inline">
      <xsl:choose>
        <!--
          <xsl:when test = "@character-style">
              <xsl:element name = "phrase">
                <xsl:attribute name = "rend">
                    <xsl:value-of select = "@character-style"/>
                </xsl:attribute>
                <xsl:apply-templates/>
              </xsl:element>
          </xsl:when>
          -->
          <xsl:when test = "@footnote-marker"/>
              
          <xsl:when test = "(@italics = 'true') and (@bold = 'true') and (@underlined)">
              <emph rend = "bold-italic-underlined">
                 <xsl:apply-templates/> 
              </emph>
          </xsl:when>
          <xsl:when test = "(@italics = 'true') and (@bold = 'true')">
              <emph rend = "bold-italic">
                 <xsl:apply-templates/> 
              </emph>
          </xsl:when>
          <xsl:when test = "(@italics = 'true') and (@underlined)">
              <emph rend = "italic-underlined">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "(@bold = 'true') and (@underlined)">
              <emph rend = "bold-underlined">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "@italics = 'true'">
              <emph rend = "italics">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "@bold = 'true'">
              <emph rend = "bold">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "@underlined">
                <xsl:choose>
                    <xsl:when test = "@underlined = 'false'">
                        <xsl:call-template name = "inline-off"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <emph rend  = "underlined">
                            <xsl:apply-templates/>
                        </emph>
                    </xsl:otherwise>
                </xsl:choose>
          </xsl:when>
          <xsl:when test = "@strike-through = 'true'">
            <emph rend = "strike-through">
                <xsl:apply-templates/>
            </emph>
          </xsl:when>
          <xsl:when test = "@double-strike-through = 'true'">
              <emph rend = "double-strike-through">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "@superscript = 'true'">
              <emph rend = "superscript">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "@subscript = 'true'">
              <emph rend = "subscript">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "@shadow = 'true'">
              <emph rend = "shadow">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "@outline = 'true'">
              <emph rend = "outline">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "@emboss = 'true'">
              <emph rend = "emboss">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "@font-color = 'true'">
                <xsl:element name = "emph">
                    <xsl:attribute name = "rend">
                        <xsl:text>font-color:</xsl:text>
                        <xsl:value-of select = "@font-color"/>
                    </xsl:attribute>
                  <xsl:apply-templates/>
                </xsl:element>
          </xsl:when>
          <xsl:when test = "@engrave = 'true'">
              <emph rend = "engrave">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "@small-caps = 'true'">
              <emph rend = "small-caps">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "@caps = 'true'">
              <emph rend = "caps">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "@hidden = 'true'">
              <emph rend = "hidden">
                  <xsl:apply-templates/>
              </emph>
          </xsl:when>
          <xsl:when test = "@font-size ">
                <xsl:element name = "emph">
                    <xsl:attribute name = "rend">
                        <xsl:text>font-size:</xsl:text>
                        <xsl:value-of select = "@font-size"/>
                    </xsl:attribute>
                  <xsl:apply-templates/>
                </xsl:element>
          </xsl:when>
          <xsl:when test = "@font-style ">
                <xsl:element name = "emph">
                    <xsl:attribute name = "rend">
                        <xsl:text>font-style:</xsl:text>
                        <xsl:value-of select = "@font-style"/>
                    </xsl:attribute>
                  <xsl:apply-templates/>
                </xsl:element>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name = "inline-off"/>
          </xsl:otherwise>
      </xsl:choose>
    </xsl:template>

    <!--handle off inline-->
    <xsl:template name = "inline-off">
        <xsl:choose>
            <xsl:when test = "(../@italics = 'true') 
                or (../@bold = 'true') 
                or (../@strike-through = 'true') 
                or (../@underlined) 
                or (../@strike-through = 'true')
                or (../@double-strike-through = 'true')
                or (../@emboss = 'true')
                or (../@shadow = 'true')
                or (../@outline = 'true')
                or (../@caps = 'true')
                or (../@small-caps = 'true')
                or (../@hidden = 'true')
                ">
                <emph rend = "paragraph-emph-off">
                    <xsl:apply-templates/>
                </emph>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!--Fields-->

    <xsl:template match = "rtf:field">
        <xsl:choose>
            <xsl:when test = "@type = 'index-entry'">
                <xsl:element name = "index">
                    <xsl:attribute name = "level1">
                       <xsl:value-of select = "@main-entry"/> 
                    </xsl:attribute>
                    <xsl:if test = "@sub-entry">
                        <xsl:attribute name = "level2">
                           <xsl:value-of select = "@sub-entry"/> 
                        </xsl:attribute>
                    </xsl:if>
                </xsl:element>
            </xsl:when>
        </xsl:choose>
    </xsl:template>


    <!--headers and footers-->

    <xsl:template match = "rtf:header-or-footer"/>

</xsl:stylesheet>


<!--









-->


